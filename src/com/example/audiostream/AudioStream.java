package com.example.audiostream;

import java.io.File;
import java.util.ArrayList;
import java.util.Timer;

import android.app.Activity;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Toast;

/**
 * AudioSream is the Main class
 * 
 * @author SourceBits
 * 
 * @version 1
 * 
 */
public class AudioStream extends Activity implements OnClickListener {

	/**
	 * mNext button plays the next song
	 */
	private Button mNext;

	/**
	 * mPrev button plays the next song
	 */
	private Button mPrev;

	/**
	 * counter for track of current song
	 */
	private int count = 1;

	/**
	 * Flag to pause and play the song
	 */
	private boolean flag = true;

	/**
	 * mMediaPlayer is the media player object
	 */
	private MediaPlayer mMedaiPlayer;

	/**
	 * mAlbumPic Displays the album picture
	 */
	private ImageView mAlbumPic;

	/**
	 * songs
	 */
	private ArrayList<String> mSongs;

	/**
	 * seek bar to indicate the video
	 */
	private SeekBar videoControl;
	/**
	 * seek bar to indicate the video
	 */
	private Async async;
	/**
	 * current postion of the media player
	 */
	private int current = 0;
	/**
	 * max media player durations
	 */
	private int max = 100;
	private boolean autoSeek = true;
	private View dashboardButtons;
	private View seekbarLayout;
	private TranslateAnimation slideup, slideDown, slideDown1, slideUp1;
	private CountDownTimer timer;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_audio_stream);
		super.onCreate(savedInstanceState);
		initilizeVariables();
		seekbarLayout.setVisibility(seekbarLayout.INVISIBLE);
		dashboardButtons.setVisibility(dashboardButtons.INVISIBLE);
		addSongstoList();
		eventhandlers();
		animationInistailization();
		async = new Async();
		async.execute();

		timer = new CountDownTimer(5000, 0) {

			@Override
			public void onTick(long millisUntilFinished) {

				Log.v("&&&", "&&&&&&OnTick&&&&");
			}

			@Override
			public void onFinish() {
				Log.v("&&&", "&&&&&&DONE&&&&");
				dashboardButtons.startAnimation(slideup);
				seekbarLayout.startAnimation(slideDown1);
				seekbarLayout.setVisibility(seekbarLayout.INVISIBLE);
				dashboardButtons.setVisibility(dashboardButtons.INVISIBLE);
			}
		};
	}

	/**
	 * Initializes the animation objects
	 */
	private void animationInistailization() {
		slideDown = new TranslateAnimation(0, 0, -100, 0);
		slideDown.setDuration(500);
		slideDown.setFillAfter(true);
		slideup = new TranslateAnimation(0, 0, 0, -100);
		slideup.setDuration(500);
		slideup.setFillAfter(true);
		slideUp1 = new TranslateAnimation(0, 0, 100, 0);
		slideUp1.setDuration(500);
		slideUp1.setFillAfter(true);
		slideDown1 = new TranslateAnimation(0, 0, 0, 100);
		slideDown1.setDuration(500);
		slideDown1.setFillAfter(true);

	}

	/**
	 * views listeners are set here
	 */
	private void eventhandlers() {
		autoSeek = true;
		mAlbumPic.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (flag == true) {
					mMedaiPlayer.pause();
					dashboardButtons.setVisibility(dashboardButtons.VISIBLE);
					seekbarLayout.setVisibility(seekbarLayout.VISIBLE);
					dashboardButtons.startAnimation(slideDown);
					seekbarLayout.startAnimation(slideUp1);
					new Thread(new Runnable() {

						@Override
						public void run() {
							timer.start();
						}
					});

				} else {
					mMedaiPlayer.start();
					dashboardButtons.startAnimation(slideup);
					seekbarLayout.startAnimation(slideDown1);
					seekbarLayout.setVisibility(seekbarLayout.INVISIBLE);
					dashboardButtons.setVisibility(dashboardButtons.INVISIBLE);
				}
				flag = !flag;
			}
		});
		mPrev.setOnClickListener(this);
		mNext.setOnClickListener(this);
		videoControl.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {

				// videoControl.setProgress(0);
				autoSeek = true;
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				if (fromUser) {
					try {

						mMedaiPlayer.seekTo(progress);
						Log.v("setTo", progress + "");
						videoControl.setProgress(progress);
					} catch (Exception ee) {
					}
				}
			}
		});
		play();

	}

	/**
	 * All views are initislaised here
	 */
	private void initilizeVariables() {
		mSongs = new ArrayList<String>();
		mMedaiPlayer = new MediaPlayer();
		mPrev = (Button) findViewById(R.id.button1);
		mNext = (Button) findViewById(R.id.button2);
		videoControl = (SeekBar) findViewById(R.id.seek1);
		mAlbumPic = (ImageView) findViewById(R.id.imageView1);
		seekbarLayout = findViewById(R.id.seekBarLayout);
		dashboardButtons = findViewById(R.id.DashBoardLayouts);

	}

	/**
	 * add all the songs' path the ArrayList
	 */
	void addSongstoList() {
		for (File file : new File("/sdcard/music").listFiles()) {
			if (file.getName().endsWith(".mp3")) {
				String name = file.getName();
				mSongs.add(name);
			}
		}
	}

	@Override
	public void onClick(View v) {
		/**
		 * handles the mPrev button click
		 */
		if (v.getId() == R.id.button1) {
			if (count > 1) {
				count--;
				Log.v("mPREV---COUNT", count + "");
				mMedaiPlayer.stop();
				mMedaiPlayer.reset();
				play();
			}
		} else
		/**
		 * handles the mNext button click
		 */
		if (v.getId() == R.id.button2) {

			if (count < mSongs.size() - 1) {
				count++;
				Log.v("mNext----COUNT", count + "");
				mMedaiPlayer.stop();
				mMedaiPlayer.reset();
				play();
			}
		}

	}

	/**
	 * set the path of the file to be played and plays the cong
	 */
	void play() {
		String PATH = Environment.getExternalStorageDirectory()
				.getAbsolutePath();

		Log.v("PLAYING", "" + mSongs.get(count) + "COUNT:" + count);

		try {
			mMedaiPlayer.setDataSource(PATH + "/music/" + mSongs.get(count));
			mMedaiPlayer.prepare();
			mMedaiPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
			mMedaiPlayer.start();
			current = mMedaiPlayer.getDuration() / 100000000;
			max = videoControl.getMax();
			Toast.makeText(AudioStream.this, "Playing " + mSongs.get(count),
					Toast.LENGTH_SHORT).show();
		} catch (Exception ee) {
			Log.v("ERROR", "");
		}

	}

	/**
	 * Releses the media player object back
	 */
	@Override
	protected void onDestroy() {
		mMedaiPlayer.reset();
		mMedaiPlayer.release();
		super.onDestroy();
	}

	/**
	 * AsyncTask move the progress bar
	 * 
	 * @author cynogn
	 * 
	 */
	class Async extends AsyncTask<Void, Long, Void> {
		@Override
		protected void onProgressUpdate(Long... values) {
			if (autoSeek == true) {
				videoControl.setClickable(false);
				videoControl.setProgress((int) (values[0] / 1000));
				videoControl.setClickable(true);
				super.onProgressUpdate(values);
			}
		}

		@Override
		protected void onPostExecute(Void result) {

			super.onPostExecute(result);
		}

		@Override
		protected Void doInBackground(Void... params) {

			while (true) {
				try {
					Thread.sleep(1000);
					publishProgress((long) mMedaiPlayer.getCurrentPosition());
					current = mMedaiPlayer.getCurrentPosition() / 1000;
				} catch (Exception ee) {
				}
			}
		}
	}
}
